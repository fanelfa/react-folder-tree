import { useState } from 'react';
import './App.css';

import Tree from './components/FolderTree'

const folders = [
  {
    key: '1',
    title: 'Root',
    children: [
      {
        key: '1-1',
        title: 'Children 1',
        children: [
          {
            key: '1-1-1',
            title: 'Children 1-1'
          },
        ]
      },
      {
        key: '1-2',
        title: 'Children 2'
      },
    ]
  }
]

function App() {

  const [selectedFolder, setSelectedFolder] = useState();

  const onClickFolder = (node) => {
    setSelectedFolder(node);
  }
  return (
    <div className="App">
      <div className="Tree">
        <Tree
          data={folders}
          onClickFolder={onClickFolder}
        // onChangeSelectedKeys={onChangeSelectedKeys}
        // onChangeExpandedKeys={onChangeExpandedKeys}
        // selectedKeys={state.selectedKeys}
        // expandedKeys={state.expandedKeys}
        />
      </div>
      <div className="SelectedFolder">
        <span>Selected Folder :</span>
        {
          selectedFolder && <pre>
            {JSON.stringify(selectedFolder, undefined, 2)}
          </pre>
        }
      </div>
    </div>
  );
}

export default App;
