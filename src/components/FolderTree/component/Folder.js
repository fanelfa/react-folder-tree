import React, { useState, useEffect, memo } from 'react'
import { FiFolder } from 'react-icons/fi'
import { BiChevronRight } from 'react-icons/bi'
import { useTreeContext } from '../context/TreeContext'

import classes from '../FolderTree.module.css'

const Folder = ({
   id,
   name = '',
   hasChildren = true,
   node = {},
   children,
}) => {
   const [isOpen, setIsOpen] = useState(false)
   const [wasOpened, setWasOpened] = useState(false)

   const { state, actions, onClickFolder, onExpand } = useTreeContext()

   useEffect(() => {
      if (state.expandedKeys.includes(id)) {
         setIsOpen(true)
         !wasOpened && setWasOpened(true)
      } else {
         isOpen && setIsOpen(false)
      }
      // eslint-disable-next-line
   }, [state.expandedKeys])

   const _onExpand = node => {
      actions.setExpandedKey({ key: id, isPop: state.expandedKeys.includes(id) })
      onExpand(node)
   }

   const handleToggle = e => {
      e.preventDefault()
      e.stopPropagation()
      !state.expandedKeys.includes(id) && actions.setExpandedKey({ key: id, isPop: state.expandedKeys.includes(id) })
      actions.setSelectedKey({ key: id })
      onClickFolder(node)
   }

   return (
      <div className={classes.Folder}>
         <div
            className={`${classes.Title} ${state.selectedKeys.includes(id) ? classes.Active : ''}`}
            onClick={handleToggle}
            title={name}
         >
            <div
               className={classes.IconExpand + (hasChildren ? ` ${classes.HasChildren}` : '')}
               onClick={e => {
                  if (hasChildren) {
                     e.stopPropagation()
                     _onExpand(node)
                  }
               }}
            >
               {
                  // !hasChildren ? null :
                  //    !isOpen ?
                  //       <BiChevronRight /> :
                  //       <BiChevronDown />
               }
               {
                  hasChildren && <BiChevronRight
                     className={classes.Icon}
                     style={isOpen ? {
                        transform: 'rotate(90deg)'
                     }:{}}
                  />
               }
            </div>
            <FiFolder className={classes.IconFolder} />
            <span>{name}</span>
         </div>
         {
            // wasOpened && <Collapsible isOpen={isOpen}>{children}</Collapsible>
         }
         <Collapsible isOpen={isOpen}>{children}</Collapsible>
      </div>
   )
}

const Collapsible = ({ isOpen = false, children }) => <div
   className={classes.Collapse + (isOpen ? ` ${classes.Active}` : '')}
>{children}</div>

export default memo(Folder, (np, pp) => {
   return pp.id === np.id && pp.name === np.name && pp.hasChildren === np.hasChildren
})