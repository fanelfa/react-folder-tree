import React, {useEffect, memo} from 'react'
import { useTreeContext } from '../context/TreeContext'
import Folder from './Folder'

const TreeRecursive = ({ 
   data = [],
   onChangeSelectedKeys=(keys)=>{},
   onChangeExpandedKeys=(keys)=>{},
}) => {
   const {state} = useTreeContext()

   useEffect(()=>{
      onChangeSelectedKeys(state.selectedKeys)
      // eslint-disable-next-line
   },[state.selectedKeys])
   useEffect(()=>{
      onChangeExpandedKeys(state.expandedKeys)
      // eslint-disable-next-line
   },[state.expandedKeys])

   // loop through the data
   return (data||[]).map(item => {
      // if its a file render <File />
      // if (item.type === "file") {
      //    return <File name={item.name} />
      // }
      // if its a folder render <Folder />
      // if (item.type === "folder") {
      return (
         <Folder 
            name={item.title}
            key={item.key}
            id={item.key}
            hasChildren={item.children?.length>0}
            node={item}
         >
            {/* Call the <TreeRecursive /> component with the current item.childrens */}
            {
               item.children?.length > 0 && <TreeRecursive data={item.children} />
            }
         </Folder>
      )
      // }
      // else return <div></div>
   })
}

export default memo(TreeRecursive, (np, pp)=>pp.data===np.data)