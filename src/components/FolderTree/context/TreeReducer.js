export const SET_SELECTED_KEY = 'set selected key'
export const SET_EXPANDED_KEY = 'set expanded key'
export const SET_SELECTED_KEYS = 'set selected keys'
export const SET_EXPANDED_KEYS = 'set expanded keys'
export const SET_VALUE = 'set value'

export const initialState = {
   data: [],
   selectedKeys: [],
   expandedKeys: [],
}

export const treeReducer = (state=initialState, action) => {
   const {type, payload} = action

   switch(type){
      case SET_SELECTED_KEY: return {
         ...state,
         selectedKeys: [payload.key],
      }
      case SET_EXPANDED_KEY: return {
         ...state,
         expandedKeys: payload.isPop ?
            state.expandedKeys.filter(it => it !== payload.key) :
            [...state.expandedKeys, payload.key],
      }
      case SET_EXPANDED_KEYS: return {
         ...state,
         expandedKeys: payload.keys,
      }
      case SET_SELECTED_KEYS: return {
         ...state,
         selectedKeys: payload.keys,
      }
      case SET_VALUE: return {
         ...state,
         [payload.name]: payload.value,
      }
      default: return state
   }
}

export const useActions = dispatch => ({
   setSelectedKey: ({key}) => dispatch({type: SET_SELECTED_KEY, payload: {key}}),
   setExpandedKey: ({key, isPop=false}) => dispatch({type: SET_EXPANDED_KEY, payload: {key, isPop}}),
   setSelectedKeys: ({ keys }) => dispatch({ type: SET_SELECTED_KEYS, payload: { keys } }),
   setExpandedKeys: ({ keys }) => dispatch({ type: SET_EXPANDED_KEYS, payload: { keys } }),
   setValue: ({ name, value }) => dispatch({ type: SET_VALUE, payload: { name, value }}),
})