import React, { useEffect } from 'react'
import { initialState, treeReducer, useActions } from './TreeReducer'

const TreeContext = React.createContext()
TreeContext.displayName = 'Tree Context'

export const useTreeContext = () => React.useContext(TreeContext)

const getListFolder = (data = []) => {
   let result = []
   const folderFactory = (id = '', parent_id = '') => ({ id, parent_id })
   const getParrent = (id = '', _children = []) => {
      _children.forEach(it => {
         result.push(folderFactory(it.key, id))
         if (it.children?.length > 0) {
            getParrent(it.key, it.children)
         }
      })
   }
   getParrent('root', data)
   return result
}

const getAllParent = (id=0, listFolder = []) => {
   let copyFolder = listFolder
   let result = [id]
   let nextId = id
   do {
      const nId = nextId
      const current = copyFolder.find(it => it.id === nId)
      if (current?.parent_id) {
         nextId = current.parent_id
         // copyFolder = copyFolder.filter(it => it.id !== current.id)
         result.push(current.parent_id)
      } else {
         nextId = null
      }
   } while (nextId !== null)
   return result
}

export const TreeProvider = ({
   onClickFolder = (node) => { },
   onExpand = (node) => { },
   selectedKeys = [],
   expandedKeys = [],
   data = [],
   children,
}) => {
   const [state, dispatch] = React.useReducer(treeReducer, initialState)
   const actions = useActions(dispatch)

   useEffect(() => {
      if (expandedKeys?.length) {
         if (expandedKeys?.length === 1) {
            const listExpanded = getAllParent(expandedKeys[0], (state.data || []))
            actions.setExpandedKeys({ keys: listExpanded.filter(it=>it!=='root') })
         } else {
            actions.setExpandedKeys({ keys: expandedKeys })
         }
      }
      // eslint-disable-next-line
   }, [expandedKeys])

   useEffect(() => {
      selectedKeys?.length && actions.setSelectedKeys({ keys: selectedKeys })
      // eslint-disable-next-line
   }, [selectedKeys])

   useEffect(() => {
      if (data?.length > 0) {
         const listData = getListFolder(data)
         actions.setValue({
            name: 'data',
            value: listData,
         })
      }
      // eslint-disable-next-line
   }, [data])

   return <TreeContext.Provider value={{
      actions,
      state,
      onClickFolder,
      onExpand
   }}>
      {children}
   </TreeContext.Provider>
}