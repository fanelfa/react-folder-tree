import React from 'react'
import TreeRecursive from './component/TreeRecursive'
import { TreeProvider } from './context/TreeContext'

import classes from './FolderTree.module.css'

const Tree = ({
   data = [],
   onClickFolder = (node) => { },
   onExpand = (node) => {},
   selectedKeys = [],
   expandedKeys = [],
   onChangeSelectedKeys = (keys) => { },
   onChangeExpandedKeys = (keys) => { },
   children,
}) => {
   const isImparative = data && !children

   return <div className={classes.Tree}>
      <TreeProvider
         onClickFolder={onClickFolder}
         onExpand={onExpand}
         expandedKeys={expandedKeys}
         selectedKeys={selectedKeys}
         data={data}
      >
         {
            isImparative ?
               <TreeRecursive
                  data={data}
                  onChangeSelectedKeys={onChangeSelectedKeys}
                  onChangeExpandedKeys={onChangeExpandedKeys}
               /> :
               children
         }
      </TreeProvider>
   </div>
}

export default React.memo(Tree)